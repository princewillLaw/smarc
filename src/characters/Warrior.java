package characters;

import java.util.ArrayList;
import java.util.Arrays;

import spells.*;


public class Warrior extends Player{
	
	public Warrior(String name,int team) {
		super(name,0,team,400,250,6,3,65,
				0,0,25,45,"Warrior",1.0,false,
				new ArrayList<>(Arrays.asList(new Guardian(),
						new LongSword(), new Spearce(), new Berserk())),9);
		// TODO Auto-generated constructor stub
	}
	
	public int getDefaultSpeed() {
		return super.getSpeedLeft() >= 3 ? 3 : super.getSpeedLeft();
	}
}
