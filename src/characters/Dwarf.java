package characters;

import java.util.ArrayList;
import java.util.Arrays;

import spells.Grind;
import spells.Shock;
import spells.Upgrade;
import spells.WarCry;


public class Dwarf extends Player{
	
	public Dwarf(String name,int team) {
		super(name,0,team,500,150,3,2,80,
				0,0,10,40,"Dwarf",1.0,false,
				new ArrayList<>(Arrays.asList(new WarCry(), new Upgrade(),
						new Grind(), new Shock())),6);
		// TODO Auto-generated constructor stub
	}
	
	public int getDefaultSpeed() {
		return super.getSpeedLeft() >= 2 ? 2 : super.getSpeedLeft();
	}

}
