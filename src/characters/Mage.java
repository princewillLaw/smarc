package characters;

import java.util.ArrayList;
import java.util.Arrays;

import spells.*;


public class Mage extends Player{
	
	public Mage(String name,int team) {
		super(name,0,team,300,300,4,5,35,
				0,0,20,50,"Mage",1.0,false,
				new ArrayList<>(Arrays.asList(new ElementalBlast(), new Heal(), new Teleport(), new FireBlast(), new EarthBlast(), new WindBlast(),
						new IceBlast())),5);
		// TODO Auto-generated constructor stub
	}
	
	public int getDefaultSpeed() {
		return super.getSpeedLeft() >= 2 ? 2 : super.getSpeedLeft();
	}
	
}
