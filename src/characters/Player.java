package characters;

import java.util.ArrayList;
import java.util.Random;

import spells.*;

public class Player{
	
	private String Name;
	private int Id;
	private int Hp;
	private int Magic;
	private int Movement;
	private int AttackRange;
	private int AttackDamage;
	private int PositionX;
	private int PositionY;
	
	private int Rest;
	private int Charge;
	
	private String Type;
	
	private double DamageVariation;
	private boolean isHpFinish;
	private boolean isPlayer = true;
	
	private int teamNumber = -1;
	private int speed;
	private int speedLeft;
	private int bleedDamage = 0;
	private int defaultSpeed = 0;
	
	private ArrayList<Spell> spells = new ArrayList<>();
	
	public Player() {}
	
	public Player(String name, int id, int teamNumber, int hp, int magic, int movement, int attackRange, int attackDamage,
			int positionX, int positionY, int rest, int charge, String type, double damageVariation, boolean isHpFinish,
			ArrayList<Spell> spells,int speed) {
		
		Name = name;
		Id = id;
		Hp = hp;
		Magic = magic;
		Movement = movement;
		AttackRange = attackRange;
		AttackDamage = attackDamage;
		Rest = rest;
		Charge = charge;
		Type = type;
		DamageVariation = damageVariation;
		this.isHpFinish = isHpFinish;
		this.spells = spells;
		this.teamNumber = teamNumber;
		this.speed = speed;
		speedLeft = speed;
		
		PositionX = new Random().nextInt(24)+1;
		PositionY = new Random().nextInt(24)+1;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getId() {
		return Id;
	}

	public void setId(int iD) {
		Id = iD;
	}

	public int getHp() {
		return Hp;
	}

	public void setHp(int hpChange) {
		if(hpChange < 0) {
			Hp += ((hpChange + getBleedDamage()) * getDamageVariation());
			increaseBleedDamage();
		}else
			Hp += hpChange;
		
		setHpCap();
	}
	
	public void pureSetHp(int newHp) {
		Hp = newHp;
		setHpCap();
	}
	
	private void setHpCap() {
		if(Hp<=0)
			setHpFinish(true);
		if(Hp>500)
			Hp = 500;
	}

	public boolean isHpFinish() {
		return isHpFinish;
	}

	public void setHpFinish(boolean isHpFinish) {
		this.isHpFinish = isHpFinish;
	}

	public int getMagic() {
		return Magic;
	}

	public void setMagic(int magicChange) {
		Magic += magicChange;
		setMagicCap();
	}
	
	public void pureSetMagic(int newMagic) {
		Magic = newMagic;
		setMagicCap();
	}
	
	private void setMagicCap() {
		if(Magic<0)
			Magic = 0;
		if(Magic>500)
			Magic = 500;
	}

	public int getMovement() {
		return Movement;
	}

	public void setMovement(int mOVEMENT) {
		Movement += mOVEMENT;
	}
	public void pureSetMovement(int movement) {
		Movement = movement;
	}

	public int getAttackRange() {
		return AttackRange;
	}

	public void setAttackRange(int aTTACK_RANGE) {
		AttackRange += aTTACK_RANGE;
	}

	public int getAttackDamage() {
		return AttackDamage;
	}

	public void setAttackDamage(int aTTACK_DAMAGE) {
		AttackDamage += aTTACK_DAMAGE;
	}

	public int getPositionX() {
		return PositionX;
	}
	
	public void addPositionX(int posOffset) {
		if (PositionX+posOffset > 24)
			PositionX = 24;
		else if(PositionX+posOffset < 1)
			PositionX = 1;
		else
			PositionX += posOffset;
	}

	public void setPositionX(int pOSITION_X) {
		if (pOSITION_X > 24)
			pOSITION_X = 24;
		else if(pOSITION_X < 1)
			pOSITION_X = 1;
		
		PositionX = pOSITION_X;
	}

	public int getPositionY() {
		return PositionY;
	}
	
	public void addPositionY(int posOffset) {
		if (PositionY+posOffset > 24)
			PositionY = 24;
		else if(PositionY+posOffset < 1)
			PositionY = 1;
		else
			PositionY += posOffset;
	}

	public void setPositionY(int pOSITION_Y) {
		if (pOSITION_Y > 24)
			pOSITION_Y = 24;
		else if(pOSITION_Y < 1)
			pOSITION_Y = 1;
		
		PositionY = pOSITION_Y;
	}

	public String getType() {
		return Type;
	}

	public void setType(String tYPE) {
		Type = tYPE;
	}
	
	public int getRest() {
		return Rest;
	}

	public void setRest(int rEST) {
		Rest += rEST;
	}

	public int getCharge() {
		return Charge;
	}

	public void setCharge(int cHARGE) {
		Charge += cHARGE;
	}
	

	public double getDamageVariation() {
		return DamageVariation;
	}

	public void setDamageVariation(double dAMAGE_VARIATION) {
		DamageVariation += dAMAGE_VARIATION;
	}

	public ArrayList<Spell> getSpells() {
		return spells;
	}

	public void setSpells(ArrayList<Spell> spells) {
		this.spells = spells;
	}
	
	public int getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(int teamNumber) {
		this.teamNumber = teamNumber;
	}
	

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getBleedDamage() {
		return bleedDamage;
	}

	public void increaseBleedDamage() {
		if(this.bleedDamage <= 0) {
			setPureBleedDamage(1);
		}else {
			this.bleedDamage *= 2;
		}
	}
	
	public void setPureBleedDamage(int bleedDamage) {
		this.bleedDamage = bleedDamage;
	}

	public void rest() {
		setHp(getRest());
		setPureBleedDamage(0);
	}
	
	public void charge() {
		setMagic(getCharge());
	}
	
	public int getDefaultSpeed() {
		return getSpeedLeft();
	}
	
	public int getSpeedLeft() {
		return speedLeft;
	}
	
	public void setSpeedLeft(int speedLeft) {
		this.speedLeft = speedLeft;
	}
	
	public boolean isPlayer() {
		return isPlayer;
	}

	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}

	public String status(){
		return String.format("%d. %s %s\nHp: %d Magic: %d X: %d Y: %d DMG: %d RNG: %d VRN: %.2f MVT: %d",getId(),(getName()+" the "+getType()),"Team "+getTeamNumber(),getHp(),getMagic(),getPositionX(),getPositionY(),getAttackDamage(),getAttackRange(),getDamageVariation(),getMovement());
	}
	
	

}
