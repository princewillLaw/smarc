package Bots;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import characters.Mage;
import characters.Player;
import console_version.Guides;
import console_version.SlotAction;
import spells.Spell;

public class MageBot implements Runnable {

	  // The client socket
	  private static Socket clientSocket = null;
	  private static BufferedReader is=null;
	  private static PrintStream os = null;
	  public static ObjectMapper mapper;

	  private static boolean closed = false;
	  private static boolean serverWaiting = false;
	  
	  private static Guides guide = new Guides();
	  public static Player player;
	  public static ArrayList<Player> players = null;
	  private static ArrayList<ArrayList<SlotAction>> botAction = new ArrayList<ArrayList<SlotAction>>();
	  private static int turn = -1;
	  
	  
	  public static void main(String[] args) {
		  
  
	    // get host IP.
	    System.out.println("Enter server IP Address");
	    String host = "localhost";

//	    if (args.length < 2) {
//	    	guide.getClientInfo(portNumber,host);
//	    } else {
//	      host = args[0];
//	      portNumber = Integer.valueOf(args[1]).intValue();
//	    }

	    /*
	     * Open a socket on a given host and port. Open input and output streams.
	     */
	    try {
	    	//set up connection to server thread
	      clientSocket = new Socket(host, 2222);
	      os = new PrintStream(clientSocket.getOutputStream());
	      is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	      mapper = new ObjectMapper();
	      //default player
	      String name = "MageBot"+(new Random().nextInt(575)+1);
	      player = new Mage(name,-1);
	      player.setPlayer(false);
	      
	      String[] gameInfo = is.readLine().split(",");
	      
	      //send player to server
	      String jsonPlayerString = mapper.writeValueAsString(player);
	      os.println(jsonPlayerString);
	      
	     System.out.println("Waiting for other players");
	      
	    } catch (IOException e) {
	      e.printStackTrace();
	    }

	    /*
	     * If everything has been initialized then we want to write some data to the
	     * socket we have opened a connection to on the port portNumber.
	     */
	    if (clientSocket != null && os != null && is != null) {
	        /* Create a thread to read from the server. */
	        new Thread(new MageBot()).start();
	        Thread playerTurn = new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						while (true) {
						// TODO Auto-generated method stub
							if (serverWaiting) {
								if (!player.isHpFinish()) {
									System.out.println("bot hp not finished");
									String jsonPlayerSlotActionsString = 
											mapper.writeValueAsString(getBotNextAction());
									System.out.println("bot slot action selected");
									os.println(jsonPlayerSlotActionsString);
									System.out.println("bot slot actions sent to server");
									players = null;
									serverWaiting = false;
								} else {
									break;
								}
							}else {
								Thread.sleep(500);
							}
						}
						
				        // Close the output stream, close the input stream, close the socket.
				        os.close();
				        is.close();
				        clientSocket.close();
						
					}catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					
				}
			});
	        playerTurn.start();
	        //running the next turn until no next turn
	    }
	  }

	  /*
	   * Create a thread to read from the server. (non-Javadoc)
	   * 
	   * @see java.lang.Runnable#run()
	   */
	  public void run() {
	    /*
	     * Keep on reading from the socket 
	     */
		    try {
		    	String jsonServerPlayersString = is.readLine();
		    	if(jsonServerPlayersString.startsWith("BroadCast:")) {
		    		System.out.println(jsonServerPlayersString);
		    		if(jsonServerPlayersString.contains(player.getName() + " You Lose!")) {
		    			player.setHpFinish(true);
		    			serverWaiting = true;
		    		}
		    	}else {
					players = mapper.readValue(jsonServerPlayersString, new TypeReference<ArrayList<Player>>(){});
					//TODO: figure out a way to assign player id better
					
					for(Player p:players) {
						if(p.getName().equals(player.getName()) 
								&& p.getType().equals(player.getType())) {
							player = p;
						}
					}
					serverWaiting = true;
		    	}
				run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
	  }
	  
	  public static ArrayList<SlotAction> getBotNextAction(){
		  ArrayList<SlotAction> sActions = new ArrayList<SlotAction>();
		  //setup recepients
		  
		  ArrayList<Player> EnemyPlayers = new ArrayList<>();
		  ArrayList<Player> AllyPlayers = new ArrayList<>();
		  for(Player p:players) {
			  if(player.getTeamNumber() == p.getTeamNumber()) {
				  AllyPlayers.add(p);
			  }else {
				  EnemyPlayers.add(p);
			  }
		  }
		  Player allyRecepient = AllyPlayers.get(new Random().nextInt(AllyPlayers.size()));
		  Player enemyRecepient = EnemyPlayers.get(new Random().nextInt(EnemyPlayers.size()));
		  
		  //setup smarc choice index with smarc rules
		  ArrayList<Integer> choiceIndexes = new ArrayList<>(3);
		  for(int i=0;i<3;i++) {
			  int choiceIndex = new Random().nextInt(5);
			  if(choiceIndexes.contains(choiceIndex)) {
				  i--;
			  }else {
				  choiceIndexes.add(choiceIndex);
			  }
		  }
		  
		  //run through choice indexes and get random slotactions
		  for (int i=0;i<choiceIndexes.size();i++) {
				switch (choiceIndexes.get(i)) {
			//cast a spell	
			case 0:
				int mind = new Random().nextInt(2);
				if(mind == 0)
					sActions.add(new SlotAction(player,enemyRecepient,'s',0,0,"Elemental Blast",i,player.getDefaultSpeed()));
				else
					sActions.add(new SlotAction(player,allyRecepient,'s',0,0,"Heal",i,player.getDefaultSpeed()));	
				break;
			//mount an attack	
			case 1:
				sActions.add(new SlotAction(player,enemyRecepient,'a',0,0,"",i,player.getDefaultSpeed()));
				break;
			//move somewhere
			case 2:
				int xMovement = new Random().nextInt(player.getMovement());
				int yMovement = player.getMovement() - xMovement;
				
				int polar = new Random().nextInt(2);
				if(polar == 0) {
					xMovement *= -1;
				}
				
				polar = new Random().nextInt(2);
				if(polar == 1) {
					yMovement *= -1;
				}
				sActions.add(new SlotAction(player,null,'m',xMovement,yMovement,"",i,player.getDefaultSpeed()));
				break;
			//rest	
			case 3:
				sActions.add(new SlotAction(player,null,'r',0,0,"",i,player.getDefaultSpeed()));
				break;
			//charge	
			case 4:
				sActions.add(new SlotAction(player,null,'c',0,0,"",i,player.getDefaultSpeed()));
				break;
			//rest	
			default:
				sActions.add(new SlotAction(player,null,'r',0,0,"",i,player.getDefaultSpeed()));
				break;
			}
		}
		botAction.add(sActions);
		turn++;
		System.out.println("Turn "+turn);
		return botAction.get(turn);
	  }
}
