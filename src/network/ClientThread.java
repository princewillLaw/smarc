package network;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import characters.Dwarf;
import characters.Mage;
import characters.Player;
import characters.Warrior;
import console_version.*;

public class ClientThread extends Thread{

	Guides guide = new Guides();
	Gameplay main = new Gameplay();
	
	ObjectMapper mapper = new ObjectMapper();

    private BufferedReader is=null;
    PrintStream os = null;
	private Socket clientSocket = null;
	private ClientThread[] threads;
	private int maxClientsCount;
	private int threadIndex;
	public boolean threadReady = false;

  	public ClientThread(Socket clientSocket, ClientThread[] threads,int threadIndex) {
	    this.clientSocket = clientSocket;
	    this.threads = threads;
	    maxClientsCount = threads.length;
	    this.threadIndex = threadIndex;
  	}

  	public void run() {
    int maxClientsCount = this.maxClientsCount;
    ClientThread[] threads = this.threads;

    try {
      /*
       * Create input and output streams for this client.
       */
      os = new PrintStream(clientSocket.getOutputStream());
      is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
      
      //Send the game mode and number of teams
      os.println(Server.GameMode+","+Server.Teams);
      
      //get new player name
	  String jsonPlayerString = is.readLine();
	  Player temp_player = mapper.readValue(jsonPlayerString, Player.class);
	  Player player = new Warrior(temp_player.getName(),-1);
	  
	  if(temp_player.getType().equals("Warrior")) 
		  player =  new Warrior(temp_player.getName(),temp_player.getTeamNumber());
	  else if(temp_player.getType().equals("Mage")) 
		  player =  new Mage(temp_player.getName(),temp_player.getTeamNumber());
	  else if(temp_player.getType().equals("Dwarf")) 
		  player =  new Dwarf(temp_player.getName(),temp_player.getTeamNumber());
	  
      player.setId(Server.playersIncrement);
      player.setPlayer(temp_player.isPlayer());
      //Game mode is not team
      if(Server.GameMode != 1) {
    	  //Everyone has a unique team number
    	  player.setTeamNumber(Server.playersIncrement);
      //Its a team game	  
      }else {
    	  //players did not input there team correctly
	      if(player.getTeamNumber() < 0) {
	    	  //stores number of players currently in the teams
			  int[] membersCount = new int[Server.Teams];
			  //defaults total players to 0
			  for(int i=0;i<Server.Teams;i++) {
				  membersCount[i] = 0;
			  }
			  //get the number of players already in a team
			  for(int i=0;i<Server.getPlayers().size();i++) {
				  membersCount[Server.getPlayers().get(i).getTeamNumber()] += 1;
			  }
			  //puts you in the team with the least players
				int lowestNumber = membersCount[0];
				int lowestIndex = 0;
				
				for(int i=0;i<membersCount.length;i++) {
					if(membersCount[i]<lowestNumber) {
						lowestIndex=i;
						lowestNumber = membersCount[i];
					}
				}
				player.setTeamNumber(lowestIndex);
	      }
	   }
          
      Server.addPlayer(player);
      Server.playersIncrement++;
      
      System.out.println(player.status());
      
      threadReady = true;
      
      //broadcast players stats to everybody
      if(Server.playersIncrement == maxClientsCount) {
    	  String jsonServerPlayersString = mapper.writeValueAsString(Server.getPlayers());
          for(int i=0;i<threads.length;i++) {
        	  if(Server.threads[i] != null)
        		  Server.threads[i].os.println(jsonServerPlayersString);
        }
          Server.clientsWaiting = false;     
      }
    
      //listen for slotAction and save it till all are received
      //process slot action and rebroadcast players stats
      while (true) {
    	  //check if everyone has sent their slot
    	  if(!Server.clientsWaiting) {
        	  try {
        		  String jsonPlayerSlotAction = "";
        		  if(Server.threads[threadIndex] != null) {
        			  jsonPlayerSlotAction = is.readLine();
        		  }else {
        			  break;
        		  }
        		 ArrayList<SlotAction> sAction = 
        				 mapper.readValue(jsonPlayerSlotAction, 
        						 new TypeReference<ArrayList<SlotAction>>(){});
      			Server.allSlotActions.add(sAction);
      			for(SlotAction sA:sAction) {
      					System.out.println(sA.toString());
      			}
      			if(!Server.allSlotActions.get(Server.getLivePlayers().size()-1).isEmpty()) {	
      				Server.clientsWaiting = true;
      				System.out.println("Gotten all slot actions");
      				if(Server.gameplay.executeSlotActions()) {
      					System.out.println("Emptying slot actions");
      					Server.allSlotActions = new ArrayList<ArrayList<SlotAction>>();
      					System.out.println("Sending new players stats");
      					for(Player p:Server.getPlayers()) {
      						System.out.println(p.status());
      					}
      					String jsonServerPlayersStringB = mapper.writeValueAsString(Server.getLivePlayers());
      					for(int i=0;i<threads.length;i++) {
      						if(Server.threads[i] != null)
      							Server.threads[i].os.println(jsonServerPlayersStringB);
      					}
      					Server.clientsWaiting = false;
      					if(Server.threads[threadIndex] == null)
      						break;
      				}else {
      					break;
      				}	
      			}
      		} catch (Exception e1) {
      			e1.printStackTrace();
      		}
    	  }

    	  try {
    		  wait(500);
    	  }catch(Exception e) {}
      }
      /*
       * Clean up. Set the current thread variable to null so that a new client
       * could be accepted by the server.
       */
      for (int i = 0; i < maxClientsCount; i++) {
        if (threads[i] == this) {
          threads[i] = null;
        }
      }

      /*
       * Close the output stream, close the input stream, close the socket.
       */
      is.close();
      os.close();
      clientSocket.close();
    } catch (Exception e) {
    	e.printStackTrace();
    } 
  }
}
