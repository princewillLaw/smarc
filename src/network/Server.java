package network;


import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import characters.Player;
import console_version.Guides;
import console_version.Gameplay;
import console_version.SlotAction;

import java.net.ServerSocket;

/*
 * A chat server that delivers public and private messages.
 */
public class Server {
	
  public static Gameplay gameplay = new Gameplay();
  private static Guides guide = new Guides();
  private static ArrayList<Player> players;
  public static ArrayList<ArrayList<SlotAction>> allSlotActions;
  public static int playersIncrement=0;
  public static boolean clientsWaiting = true;
  public static int GameMode = 2;
  public static int Teams = -1;
  public static int TeamsMaxPlayers = -1;
  
  // The server socket.
  private static ServerSocket serverSocket = null;
  // The client socket.
  private static Socket clientSocket = null;

  // This chat server can accept up to maxClientsCount clients' connections.
  public static int maxClientsCount;
  public static ClientThread[] threads;

  public static void main(String args[]) {
	  players = new ArrayList<Player>();
	  allSlotActions = new ArrayList<ArrayList<SlotAction>>();

    // The default port number.
    int portNumber = 2222;
    guide.getServerInfo(portNumber);

    try {
        System.out.println("Select Game Mode:");
        System.out.println("1. Team\n2. Free for all");
        GameMode = new Scanner(System.in).nextInt();
        if(GameMode == 1) {
        	System.out.println("How many Teams:");
    		Teams = new Scanner(System.in).nextInt();
        }
        //TODO: Make sure teams cant be more than players
        System.out.println("How many Players:");
		maxClientsCount = new Scanner(System.in).nextInt();
	
		TeamsMaxPlayers = maxClientsCount - Teams + 1;
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
		Teams = 2;
		maxClientsCount = 2;
	}finally {
		threads = new ClientThread[maxClientsCount];
	}

    /*
     * Open a server socket on the portNumber (default 2222). Note that we can
     * not choose a port less than 1023 if we are not privileged users (root).
     */
    try {
      serverSocket = new ServerSocket(portNumber);
    } catch (IOException e) {
      System.out.println(e);
    }

    /*
     * Create a client socket for each connection and pass it to a new client
     * thread.
     */
    boolean waiting = true;
    while (waiting) {
      try {
    	guide.getServerListening();
        clientSocket = serverSocket.accept();
        int i = 0;
        for (i = 0; i < maxClientsCount; i++) {
          if (threads[i] == null) {
            threads[i] = new ClientThread(clientSocket, threads,i);
            threads[i].start();
            if(i == maxClientsCount-1) {
            	serverSocket.close();
            	waiting = false;
            }
            break;
          }
        }
      } catch (IOException e) {
        System.out.println(e);
      }
    }
  }
  
  public static void broadcast(String message) {
      for(int i=0;i<threads.length;i++) {
    	  if(threads[i] != null)
    	  threads[i].os.println("BroadCast:"+message);
    }
  }



public static ArrayList<Player> getPlayers() {
	return players;
}

public static ArrayList<Player> getLivePlayers(){
	ArrayList<Player> livePlayers = new ArrayList<>();
	for(Player p:getPlayers()) {
		if(!p.isHpFinish()) {
			livePlayers.add(p);
		}
	}
	return livePlayers;
}

public static void setPlayers(ArrayList<Player> players) {
	Server.players = players;
}

public static void addPlayer(Player player) {
	Server.players.add(player);
}
}