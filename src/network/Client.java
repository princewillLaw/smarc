package network;


import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import characters.Dwarf;
import characters.Mage;
import characters.Player;
import characters.Warrior;
import console_version.Guides;
import console_version.Gameplay;

public class Client implements Runnable {

  // The client socket
  private static Socket clientSocket = null;
  private static BufferedReader is=null;
  private static PrintStream os = null;
  public static ObjectMapper mapper;

  private static boolean closed = false;
  private static boolean serverWaiting = false;
  
  private static Guides guide = new Guides();
  public static Player player;
  public static ArrayList<Player> players = null;
  private static Gameplay main = new Gameplay();
  private static int GameMode = 2;
  private static int Teams = -1;
  
  
  public static void main(String[] args) {
	  
//	//get port number
//	System.out.println("Enter server port");
//    int portNumber = new Scanner(System.in).nextInt();
//    
    // get host IP.
    System.out.println("Enter server IP Address");
    String host = "localhost";//new Scanner(System.in).nextLine();

//    if (args.length < 2) {
//    	guide.getClientInfo(portNumber,host);
//    } else {
//      host = args[0];
//      portNumber = Integer.valueOf(args[1]).intValue();
//    }

    /*
     * Open a socket on a given host and port. Open input and output streams.
     */
    try {
    	//set up connection to server thread
      clientSocket = new Socket(host, 2222);
      os = new PrintStream(clientSocket.getOutputStream());
      is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
      
      String[] gameInfo = is.readLine().split(",");
      try {
    	  GameMode = Integer.parseInt(gameInfo[0]);
    	  Teams = Integer.parseInt(gameInfo[1]);
      }catch(Exception e) {
    	  e.printStackTrace();
      }
      mapper = new ObjectMapper();
      //default player
      String name = "Anonymous"+(new Random().nextInt(575)+1);
      int selectedteam = -1;
      player = new Warrior(name,selectedteam);
      
      //get player info locally
      System.out.println(guide.getNameText());
      name = new Scanner(System.in).nextLine();
      //get new player character
      System.out.println(guide.getTypeText());
      String type = new Scanner(System.in).nextLine();
      
      //get player team
      if(GameMode == 1) {
    	  boolean correctTeam = false;
    	  //loop to get correct values
          while (!correctTeam) {
        	//Info on number of teams
			System.out.println("There are " + Teams + " teams\n"
					+ "Enter a team number to join i.e.");
			
			for(int t=1;t<Teams+1;t++) {
				System.out.println(t+". for Team "+t);
			}
			selectedteam = new Scanner(System.in).nextInt();
			//reduce to meet array index format
			try {
				selectedteam -= 1;
				//check if it falls into correct team array numbers
				if(selectedteam >-1 && selectedteam < Teams) {
					correctTeam = true;
				}else {
					//incorrect number error
					System.out.println("type a correct team number");
				}
			} catch (Exception e) {
				
			}
		}
          
      }
      //set player character from type. default to warrior if type is misspelled
      if(type.equalsIgnoreCase("Warrior"))
    	  player = new Warrior(name,selectedteam);
      else if(type.equalsIgnoreCase("Mage"))
    	  player = new Mage(name,selectedteam);
      else if(type.equalsIgnoreCase("Dwarf"))
    	  player = new Dwarf(name,selectedteam);
      
      //send player to server
      String jsonPlayerString = mapper.writeValueAsString(player);
      os.println(jsonPlayerString);
      
     System.out.println("Waiting for other players");
      
    } catch (IOException e) {
      e.printStackTrace();
    }

    /*
     * If everything has been initialized then we want to write some data to the
     * socket we have opened a connection to on the port portNumber.
     */
    if (clientSocket != null && os != null && is != null) {
 
        /* Create a thread to read from the server. */
        new Thread(new Client()).start();
        Thread playerTurn = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					while (true) {
					// TODO Auto-generated method stub
						if (serverWaiting) {
							if (!player.isHpFinish() && main.nextTurn(players, player)) {
								String jsonPlayerSlotActionsString = 
										mapper.writeValueAsString(main.getSlotActions());
								os.println(jsonPlayerSlotActionsString);
								players = null;
								serverWaiting = false;
							} else {
								break;
							}
						}else {
							Thread.sleep(500);
						}
					}
					
			        // Close the output stream, close the input stream, close the socket.
			        os.close();
			        is.close();
			        clientSocket.close();
					
				}catch (IOException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			}
		});
        playerTurn.start();
        //running the next turn until no next turn
    }
  }

  /*
   * Create a thread to read from the server. (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  public void run() {
    /*
     * Keep on reading from the socket 
     */
	    try {
	    	String jsonServerPlayersString = is.readLine();
	    	if(jsonServerPlayersString.startsWith("BroadCast:")) {
	    		System.out.println(jsonServerPlayersString);
	    		if(jsonServerPlayersString.contains(player.getName() + " You Lose!")) {
	    			player.setHpFinish(true);
	    			serverWaiting = true;
	    		}
	    	}else {
				players = mapper.readValue(jsonServerPlayersString, new TypeReference<ArrayList<Player>>(){});
				//TODO: figure out a way to assign player id better
				for(Player p:players) {
					if(p.getName().equals(player.getName()) 
							&& p.getType().equals(player.getType())) {
						player = p;
					}
				}
				serverWaiting = true;
	    	}
			run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
  }
}