package console_version;

import java.util.ArrayList;
import java.util.Scanner;

import characters.Player;
import network.Server;
import spells.Spell;
import spells.Teleport;

public class Gameplay {
	
	private Guides guide = new Guides();
	private final int slotsNumber = 3;
	private boolean isReady = false;
	private ArrayList<Character> choice;
	private ArrayList<SlotAction> slotActions;
	private ArrayList<Player> players;
	private Player player;
	
	public int turn;
	
	public Gameplay() {
		turn = 0;
	}
	
	public boolean nextTurn(ArrayList<Player> players, Player player) {
		this.players = players;
		this.player = player;
		
			//storing each player smarc choices
			choice = new ArrayList<Character>(slotsNumber);
			slotActions = new ArrayList<SlotAction>(slotsNumber);
			
			//print out player status
			System.out.println(guide.getStatusText(players));
			System.out.println(guide.getGridText(players));
			for(int slot=0;slot<slotsNumber;slot++) {
						System.out.println(guide.getSmarcSlotChoiceText(player, slot+1));
						char temp_choice =  'z';
						
						try {
							temp_choice = new Scanner(System.in).nextLine().toCharArray()[0];
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if((!choice.contains(temp_choice)) && (temp_choice == 's' || temp_choice == 'm'
								|| temp_choice == 'a' || temp_choice == 'r' || temp_choice == 'c')){
							//fetch the choices position for the player via id
							//then add to the slot index the choice selected
							choice.add(temp_choice);
						}else {
							//every one has to re enter if a mistake was made
							slot--;
							System.out.println(guide.getSmarcErrorText());
						}		
			}
			getChoices(choice);
			return true;
	}
	
	//PROCESSING SMARC FUNCTIONS
	public void executeSpell(SlotAction action) {
		for (Spell spell : Server.getPlayers().get(action.getCaster().getId()).getSpells()) {
			if (spell.getName().equals(action.getSpellName())) {
				if(spell.isCoordinateNeeded()) {
					spell.cast(action.getCaster().getId(), (action.getRecepient() != null ? action.getRecepient().getId() : -1),action.getX(),action.getY(),action.getSlot());
				}else if(spell.isRecepientNeeded()){
					spell.cast(action.getCaster().getId(), action.getRecepient().getId(),action.getSlot());
				}else {
					spell.cast(action.getCaster().getId(),action.getSlot());
				}
			}
		} 
	}
	
	private void getSpellInfo(int slot) {
		
		int selectedSpell;
		Spell spell;
		
		//get spell to cast
		while (true) {
			try {
				guide.getSelectSpell(player);
				selectedSpell = new Scanner(System.in).nextInt();
				spell = player.getSpells().get(selectedSpell);
				break;
			} catch (Exception e1) {
				guide.getSelectSpellError();
			} 
		}
		
		//get spell recepient of needed
		Player recepient = null;
		if(spell.isRecepientNeeded()) {
			guide.getSelectRecepient(players, player, "to cast spell on:");
			String recepientID = new Scanner(System.in).nextLine();
			for(Player p:players) {
				try {
					if(p.getId() == Integer.parseInt(recepientID)) {
						recepient = p;
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		//get spell coordinates if needed
		int x=0;
		int y=0;
		if(spell.isCoordinateNeeded()) {
			guide.getSelectCoordinates(new Teleport().getRange());
			String coordinates = new Scanner(System.in).nextLine();
			try {
				x = Integer.parseInt(coordinates.split(",")[0]);
				y = Integer.parseInt(coordinates.split(",")[1]);
			}catch(Exception e) {
				
			}
		}
		//get spell casting speed
		int speed = getSelectedSpeed("cast the spell");
		
		//add slot action
		slotActions.add(new SlotAction(player,recepient,'s',x,y,spell.getName(),slot,speed));
	}
	
	public void executeMove(SlotAction action) {
		Server.getPlayers().get(action.getCaster().getId()).addPositionX(action.getX());
		Server.getPlayers().get(action.getCaster().getId()).addPositionY(action.getY());
	}
		
	private void getMoveInfo(int slot) {
		int xAxis=0;
		int yAxis=0;
		for(int x=0;x<player.getMovement();x++) {
			int distanceLeft = player.getMovement() - x;
			guide.getDistanceLeft(player, distanceLeft);
			char axis = new Scanner(System.in).nextLine().toCharArray()[0];
			if(axis == 'x' || axis == 'y') {
				guide.getAxisIncrement(axis);
				String distance = new Scanner(System.in).nextLine();
				int offset = setOffset(distance,distanceLeft);
				x += Math.abs(offset)-1;
				if(offset==0) 
					guide.getAxisIncrementError();
				switch(axis) {
				case 'x':
					xAxis += offset;
					break;
				case 'y':
					yAxis += offset;
					break;
				}
			}else {
				if(axis == 's')
					break;
				guide.getAxisError();
			}
		}
		//get spell casting speed
		int speed = getSelectedSpeed("move");
		slotActions.add(new SlotAction(player,null,'m',xAxis,yAxis,"",slot,speed));
	}
	
	public void executeAttack(SlotAction action) {
		if (Server.getPlayers().get(action.getCaster().getId()).getAttackRange() >= offset_pos(Server.getPlayers().get(action.getCaster().getId()),
				Server.getPlayers().get(action.getRecepient().getId()))) {
			Server.getPlayers().get(action.getRecepient().getId())
					.setHp(-Server.getPlayers().get(action.getCaster().getId()).getAttackDamage());
		}
	}
	
	private void getAttackInfo(int slot) {
		guide.getSelectRecepient(players,player,"to attack:");
		String recepient = new Scanner(System.in).nextLine();
		
		//get spell casting speed
		int speed = getSelectedSpeed("attack");
		
		try {
			int attackeeID = Integer.parseInt(recepient);
			for(Player p:players) {
				if(p.getId() == attackeeID) {
					slotActions.add(new SlotAction(player,p,'a',0,0,"",slot,speed));
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void executeRest(SlotAction action) {
		Server.getPlayers().get(action.getCaster().getId()).rest();
	}
	
	private void getRestInfo(int slot) {
		//get spell casting speed
		int speed = getSelectedSpeed("rest");
		slotActions.add(new SlotAction(player,null,'r',0,0,"",slot,speed));
	}
	
	public void executeCharge(SlotAction action) {
		Server.getPlayers().get(action.getCaster().getId()).charge();
		Server.broadcast(guide.successfulSmarcText(action));
	}
	
	private void getChargeInfo(int slot) {
		//get spell casting speed
		int speed = getSelectedSpeed("charge");
		slotActions.add(new SlotAction(player,null,'c',0,0,"",slot,speed));
	}
	
	//PROCESSING SLOTACTION FUNCTIONS
	private void getChoices(ArrayList<Character> choice){
		
		//Set player left speed to default
		player.setSpeedLeft(player.getSpeed());
		for(int i=0;i<slotsNumber;i++) {

				switch(choice.get(i)) {
				case 's':
					getSpellInfo(i);
					break;
				case 'm':
					getMoveInfo(i);
					break;
				case 'a':
					getAttackInfo(i);
					break;
				case 'r':
					getRestInfo(i);
					break;
				case 'c':
					getChargeInfo(i);
					break;
			}
			
		}
	
	}
	
	public boolean executeSlotActions() {
		//loop through the slot number(3)
		for(int i=0;i<slotsNumber;i++) {
			//run all active spells for that slot
			runActiveSpells(i);
			//create empty unsorted array
			ArrayList<SlotAction> unsorted = new ArrayList<SlotAction>();
			//add slot actions to the unsorted array by the arrangment they entered the server
			for(ArrayList<SlotAction> playerSlots:Server.allSlotActions) {
				unsorted.add(playerSlots.get(i));
			}
			//sort the arraylist and loop through it
			for(SlotAction sAction:sortSlotActions(unsorted)) {
				if(isSlotActionPermitted(sAction)){
					//switch and execute the smart choice
					switch(sAction.getSmarcChoice()) {
					case 's':
						executeSpell(sAction);
						break;
					case 'm':
						executeMove(sAction);
						break;
					case 'a':
						executeAttack(sAction);
						break;
					case 'r':
						executeRest(sAction);
						break;
					case 'c':
						executeCharge(sAction);
						break;
					}
				}
			}
		}
		
		fixOverlappedPlayers();
		//broadcast a player was knocked out severe all connection with server
		//TODO: Allow player t still watch the match but not participate anymore
		for(int i=0;i<Server.getPlayers().size();i++) {
			if(Server.getPlayers().get(i).isHpFinish()) {
				Server.broadcast(guide.getLoserText(Server.getPlayers().get(i)));		
				Server.threads[i] = null;
			}
		}
		return isNotGameOver();
	}
	
	//HELPER FUNCTIONS
	//Just to make sure casterID exist in server players
	private ArrayList<SlotAction> sortSlotActions(ArrayList<SlotAction> unSortedSlots){
		ArrayList<SlotAction> sortedSlots = new ArrayList<SlotAction>();
		
		for(SlotAction sAction:unSortedSlots) {
			if(sortedSlots.size()<1) {
				sortedSlots.add(sAction);
			}else {
				//loop through current sorted slots
				for(int i=0;i<sortedSlots.size();i++) {
					//slot action is faster than current index
					if(sAction.getSpeed()>sortedSlots.get(i).getSpeed()) {
						sortedSlots.add(i, sAction);
						break;
					//if same speed
					}else if(sAction.getSpeed()==sortedSlots.get(i).getSpeed()) {
						//if overall speed is faster
						if(sAction.getCaster().getSpeed()>sortedSlots.get(i).getSpeed()) {
							sortedSlots.add(i, sAction);
							break;
						}
					}
					//if end of sorted list place and the end
					if(i+1 == sortedSlots.size()) {
						sortedSlots.add(sAction);
						break;
					}
				}
			}
		}
		
		return sortedSlots;
	}
	
	//check if slot action is allowed to be performed(player is still alive and is in server list)
	private boolean isSlotActionPermitted(SlotAction sAction) {
		for(Player p:Server.getLivePlayers()) {
			if(p.getId() == sAction.getCaster().getId()) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isNotGameOver() {
		//get first living player team
		int teamLeft = Server.getLivePlayers().get(0).getTeamNumber();
		//return true if 2 living players have different teams
		for(Player player:Server.getLivePlayers()) {
			if(player.getTeamNumber() != teamLeft) {
				return true;
			}
		}
		//return false if all living players are same team
		return false;
	}
	
	//returns the absolute distance between two players (x and y added)
	public int offset_pos(Player a,Player b) {
		int offset_x = Math.abs(a.getPositionX() - b.getPositionX());
		int offset_y =  Math.abs(a.getPositionY() - b.getPositionY());
		return (offset_x + offset_y);
	}
	
	//returns the move or max distance if the move is more
	public int setOffset(String distance,int maxDistance) {
		int move = 0;
		try {
			move = Integer.parseInt(distance);
			if(Math.abs(move) <= maxDistance)
				return move;
			else
				return maxDistance * (move/Math.abs(move));
		}catch(Exception e) {
			return 0;
		}
	}
	
	//runs all active spells in the game in their slot time
	public  void runActiveSpells(int slot) {
		for(int i=0;i<Server.getPlayers().size();i++) {
			for(Spell spell:Server.getPlayers().get(i).getSpells()) {
			if(spell.isActive() && spell.getCastSlot() == slot) {
				Server.broadcast(Server.getPlayers().get(i).getName()+"'s "+spell.getName()+" is active"+
						(spell.getCasterID() != spell.getRecepientID() ? " on "+
								Server.getPlayers().get(spell.getRecepientID()).getName():""));
				spell.run();		
				}
			}
		}
	}
	
	//makes sure players dont overlapp at turn end
	public void fixOverlappedPlayers() {
		for(Player player:Server.getPlayers()) {
			for(Player playerSecond:Server.getPlayers()) {
				//if not ourself
				if(player != playerSecond) {
					//they overlap
					if(player.getPositionX() == playerSecond.getPositionX() && 
							player.getPositionY() == playerSecond.getPositionY()) {
							if(playerSecond.getPositionX()<23) {
								playerSecond.addPositionX(1);
							}else if(playerSecond.getPositionX()>1) {
								playerSecond.addPositionX(-1);
							}else if(playerSecond.getPositionY()>1) {
								playerSecond.addPositionY(-1);
							}else if(playerSecond.getPositionY()>1) {
								playerSecond.addPositionY(-1);
							}
					}
				}
			}
		}
	}
	
	//retrieves speed from player
	private int getSelectedSpeed(String actionString) {
		int speed = player.getDefaultSpeed();
		try {
			guide.getSpeed(player,actionString);
			speed = new Scanner(System.in).nextInt();
			if(speed > player.getSpeedLeft()) {
				guide.getSpeedTooFastError(player);
				speed = player.getDefaultSpeed();
			}
		} catch (Exception e1) {
			guide.getSpeedError(player);
		}
		
		player.setSpeedLeft(player.getSpeedLeft()-speed);
		return speed;
	}
	
	//Getters and Setters
	public boolean isReady() {
		return isReady;
	}

	public void setReady(boolean isReady) {
		this.isReady = isReady;
	}

	public ArrayList<SlotAction> getSlotActions() {
		return slotActions;
	}

	public void setSlotActions(ArrayList<SlotAction> slotActions) {
		this.slotActions = slotActions;
	}
}
