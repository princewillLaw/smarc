package console_version;

import java.util.ArrayList;

import characters.Player;
import spells.Spell;

public class Guides{
	
	public void getServerInfo(int portNumber) {
		printText("SMARC. Server:\n"
				+ "Port number: " + portNumber);
	}
	
	public void getServerListening() {
		printText("Server is listening");
	}
	
	public void getClientInfo(int portNumber, String serverIP) {
		printText("SMARC. Client:\n"
				+ "Server: "+serverIP+"\n"
				+ "Port number: " + portNumber);
	}
	
	public void getWelcome(){
		printText("Welcome to Smarc! a semi-turn based strategy game, using the\n"
				+ "smarc. concept of player actions.");
	}
	
	public String getWelcomeText(){
		return "Welcome to Smarc! a semi-turn based strategy game, using the\n"
				+ "smarc concept of player actions.";
	}
	
	public String getNameText(){
		return "Enter your Character Name:";
	}
	
	public String getTypeText(){
		return "Enter your Character Type:\nWarrior\nMage\nDwarf";
	}
	
	public String getConnectionSuccessText(Player player){
		return whoAreYouText(player)+" connection successful";
	}
	
	public String whoAreYouText(Player player){
		return player.getName()+" the "+player.getType();
	}
	
	public String getStatusText(ArrayList<Player> players) {
		String totalStatus = "";
		for(Player player:players) {
			if(!player.isHpFinish())
				totalStatus += String.format("%s\n", player.status());
		}
		return totalStatus;
	}
	
	public String getGridText(ArrayList<Player> players) {
		String grid = "";
		for(int i=25;i>0;i--) {
			for(int j=1;j<25;j++) {
				if(i==25 && j==1) {
					grid+=" x-";
				}else if(i==25 && j==24) {
					grid+=" x+";
				}else if(i==25) {
					grid+="   ";
				}else {
					boolean isEmpty = true;
					for(Player player:players) {
						if(i == player.getPositionY() && j == player.getPositionX() && !player.isHpFinish()) {
							if(player.isPlayer()) {
								grid += player.getName().toCharArray()[0]+"."+player.getType().toCharArray()[0];
							}else {
								char[] nameChar = player.getName().toCharArray();
								grid += ""+nameChar[0]+nameChar[nameChar.length-2]+nameChar[nameChar.length-1];
							}
							isEmpty = false;
						}	
					}
					if(isEmpty)
						grid += " . ";
					if(i==24 && j==24) {
						grid+="y+";
					}
					if(i==1 && j==24) {
						grid+="y-";
					}
				}
			}
			grid +="\n";
		}
		return grid;
	}
	
	public String getSmarcSlotChoiceText(Player player,int slot) {
		return player.getName() + " select your s.m.a.r.c choice "
				+ "for slot "+slot;
	}
	
	public String getSmarcErrorText() {
		return "Wrong entry: Type any of the smarc letters (small caps)\n"
				+ "to select the corresponding smarc choice.\n"
				+ "NOTE: you can\'t have two of the same smarc choice in one turn";
	}

	public void getSelectSpell(Player player) {
		System.out.println(player.getType() + " select a spell:");
		int i= 0;
		for(Spell spell:player.getSpells()) {
			if(spell.isDisplayable())
				System.out.println(i+". "+spell.getName());		
		i++;
		}
	}
	
	public void getSpeed(Player player,String action) {
		System.out.println(player.getName() + " how fast do you want to "+action+"\n"
				+ "Your current max speed is "+player.getSpeedLeft());
	}
	
	public void getSpeedError(Player player) {
		System.out.println("Wrong Entry defaulting to: "+player.getDefaultSpeed());
	}
	
	public void getSpeedTooFastError(Player player) {
		System.out.println("You're trying to go too fast,\n"
				+ "defaulting to: "+player.getDefaultSpeed());
	}
	
	public void getSelectSpellError() {
		System.out.println("Please type the number before the spell you want to cast!");

	}
	
	public String failedSmarcText(SlotAction sA) {
		switch(sA.getSmarcChoice()) {
		case 's':
			return String.format("%s's %s %s failed", sA.getCaster().getName(),sA.getSpellName(),
					(sA.getRecepient()!=null ? "on "+sA.getRecepient().getName():""));
		case 'a':	
			return String.format("%s's attack missed %s", sA.getCaster().getName(),
					sA.getRecepient().getName());
		default:
			return "";
		}
	}
	
	public String successfulSmarcText(SlotAction sA) {
		switch(sA.getSmarcChoice()) {
		case 's':
			return String.format("%s's %s%s was successful", sA.getCaster().getName(),sA.getSpellName(),
					(sA.getRecepient()!=null ? " on "+sA.getRecepient().getName():""));
		case 'a':
			
			return String.format("%s's attack on %s was successful", sA.getCaster().getName(),
					sA.getRecepient().getName());
		case 'r':
			
			return String.format("%s rested", sA.getCaster().getName());
		case 'c':
			
			return String.format("%s charged", sA.getCaster().getName());
		default:
			return "";
		}
	}

	public void getSelectRecepient(ArrayList<Player> players,Player player,String forWhat) {
		System.out.println(player.getName() + " select id "+forWhat);
		for(Player p:players) {
			if(!p.isHpFinish())
				System.out.println(p.getId() + ". "+ p.getName());
		}
	}

	public void getRecepientError() {
		printText("Failed to cast:incorrect recepient id");
	}

	public void getDistanceLeft(Player player, int distanceLeft) {
		printText(player.getName() + ", You can move "+ distanceLeft + " more cells\n"
				+ "Type \'x\' or \'y\' to represent the axis you want to move in or \'s\' to stop moving");
	}

	public void getAxisIncrement(char axis) {
		printText("Type " + axis +" axis increment\n"
							+ "example \'3\' or \'-5\':");
	}
	
	public void getAxisIncrementError() {
		printText("Wrong entry: Whole integers only\n"
				+ "NOTE: type \'s\' to stop");
	}
	
	public void getAxisError() {
		printText("Wrong entry: Type the correct option in small caps");
	}
	
	public void getAttackMissed(Player player) {
		printText(player.getName() + " attack missed!");
	}
	
	public void getLoser(Player player) {
		printText(player.getName() + " You Lose!");
	}
	
	public String getLoserText(Player player) {
		return player.getName() + " is Out!";
	}
	
	public void getWinner(Player player) {
		printText(player.getName() + " You Won!");
	}
	
	public void printText(String text) {
		System.out.println(text);
	}

	public void getSelectCoordinates(int range) {
		// TODO Auto-generated method stub
		System.out.println("Type in the coordinates as far as "+range+" distance away\n"
				+ "E.g. coordinates \'7,5\' or \'3,6\'");
	}

}
