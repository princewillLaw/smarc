package console_version;

import characters.Player;
import network.Server;
import spells.Spell;

public class SlotAction{

		Player caster;
		Player recepient;
		char smarcChoice;
		int x;
		int y;
		String spellName;
		int slot;
		int speed;
		
		public SlotAction() {}

		public SlotAction(Player caster, Player recepient, char smarcChoice, int x, int y, String spellName,int slot,int speed) {
			super();
			this.caster = caster;
			this.recepient = recepient;
			this.smarcChoice = smarcChoice;
			this.x = x;
			this.y = y;
			this.spellName = spellName;
			this.slot = slot;
			this.speed = speed;
		}
		
		public SlotAction(Spell spell) {
			this.caster = Server.getPlayers().get(spell.getCasterID());
			this.recepient = Server.getPlayers().get(spell.getRecepientID());;
			this.smarcChoice = 's';
			this.x = 0;
			this.y = 0;
			this.spellName = spell.getName();
		}
		
		public String toString() {
			
			return String.format("Caster: %s, Recepient: %s, SMARC: %s X: %d, Y: %d, Spell: %s", 
					(getCaster() != null ? getCaster().getName() : ""),
					(getRecepient() != null ? getRecepient().getName() : ""),
					getSmarcChoice(),getX(),getY(),getSpellName());
		}
		
		public Player getCaster() {
			return caster;
		}
		public void setCaster(Player caster) {
			this.caster = caster;
		}
		public Player getRecepient() {
			return recepient;
		}
		public void setRecepient(Player recepient) {
			this.recepient = recepient;
		}
		public char getSmarcChoice() {
			return smarcChoice;
		}
		public void setSmarcChoice(char smarcChoice) {
			this.smarcChoice = smarcChoice;
		}
		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		public int getY() {
			return y;
		}
		public void setY(int y) {
			this.y = y;
		}
		public String getSpellName() {
			return spellName;
		}
		public void setSpellName(String spellName) {
			this.spellName = spellName;
		}

		public int getSlot() {
			return slot;
		}

		public void setSlot(int slot) {
			this.slot = slot;
		}

		public int getSpeed() {
			return speed;
		}

		public void setSpeed(int speed) {
			this.speed = speed;
		}
		
		
		
}
