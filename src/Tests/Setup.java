package Tests;

import java.util.ArrayList;

import characters.*;
import console_version.SlotAction;
import network.ClientThread;
import network.Server;

public class Setup {
	Player warriorA;
	Player mageA;
	Player dwarfA;
	SlotAction sAction;

	public void initialiseServerVariables() {
		Server.setPlayers(new ArrayList<>());
		
		//Initializing test players
		warriorA = new Warrior("JohnW",1);
		mageA = new Mage("JohnM",2);
		dwarfA = new Dwarf("JohnD",3);
		
		//setting up ids
		mageA.setId(0);
		warriorA.setId(1);
		dwarfA.setId(2);
		
		//setting up players position
		mageA.setPositionX(12);
		mageA.setPositionY(12);
		warriorA.setPositionX(13);
		warriorA.setPositionY(12);
		dwarfA.setPositionX(14);
		dwarfA.setPositionY(12);
	
		//adding players to server
		Server.getPlayers().add(mageA);
		Server.getPlayers().add(warriorA);
		Server.getPlayers().add(dwarfA);
		
		ClientThread[] threads = new ClientThread[0];
		Server.threads = threads;
	}
	
	public void initialiseSlotAction(char action) {
		switch(action) {
		case 's':
			sAction = new SlotAction(mageA,warriorA,'s',0,0,"Elemental Blast",1,2);
			break;
		case 'm':
			sAction = new SlotAction(mageA,null,'m',2,2,"",1,2);
			break;
		case 'a':
			sAction = new SlotAction(warriorA,mageA,'a',0,0,"",1,2);
			break;
		case 'r':
			sAction = new SlotAction(warriorA,null,'r',0,0,"",1,2);
			break;
		case 'c':
			sAction = new SlotAction(mageA,null,'c',0,0,"",1,2);
			break;
		}
	}

	public Player getWarriorA() {
		return warriorA;
	}

	public void setWarriorA(Player warriorA) {
		this.warriorA = warriorA;
	}

	public Player getMageA() {
		return mageA;
	}

	public void setMageA(Player mageA) {
		this.mageA = mageA;
	}

	public Player getDwarfA() {
		return dwarfA;
	}

	public void setDwarfA(Player dwarfA) {
		this.dwarfA = dwarfA;
	}

	public SlotAction getsAction() {
		return sAction;
	}

	public void setsAction(SlotAction sAction) {
		this.sAction = sAction;
	}
	
}
