package Tests;

import console_version.Guides;
import network.Server;

public class GridDisplay {
	
	public static void main(String[] args) {
		Setup setup = new Setup();
		setup.initialiseServerVariables();
		
		System.out.println(new Guides().getGridText(Server.getPlayers()));
	}

}
