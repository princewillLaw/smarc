package Tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import characters.Dwarf;
import characters.Mage;
import characters.Warrior;
import console_version.Gameplay;
import console_version.SlotAction;
import network.Server;
import spells.Heal;
import spells.LongSword;
import spells.Spearce;

class SmarcFunctionsTest {
	
	Setup setup;
	Gameplay gameplay = new Gameplay();

	@Test
	void testExecuteSpell() {
		setup = new Setup();
		setup.initialiseServerVariables();
		//test to see if the recipientless spell casted(took magic,changed player stat)
		SlotAction action= new SlotAction(Server.getPlayers().get(1),null,'s',0,0,"Long Sword",1,2);
		gameplay.executeSpell(action);
		//assert magic was taken for the spell
		assertEquals(new Warrior("Anon",-1).getMagic()+new LongSword().getMagic(),
				Server.getPlayers().get(1).getMagic(),
				"Magic wasnt taken for the cast of long sword");
		//assert the effect of the spell cast was applied
		assertEquals(new Warrior("Anon",-1).getAttackRange()+new LongSword().getAttackRange(),
				Server.getPlayers().get(1).getAttackRange(),
				"Attack Range wasnt given for the cast of long sword");
		//test to see if the recipient needed spell casted in range
		action= new SlotAction(Server.getPlayers().get(0),Server.getPlayers().get(0),'s',0,0,"Heal",1,2);
		gameplay.executeSpell(action);
		//assert magic was taken for spell the spell
		assertEquals(new Mage("Anon",-1).getMagic()+new Heal().getMagic(),
				Server.getPlayers().get(0).getMagic(),
				"Magic wasnt taken for the cast of Heal");
		//assert the spells effect was applied because they are within range
		assertEquals(new Mage("Anon",-1).getHp()+new Heal().getHp(),
				Server.getPlayers().get(0).getHp(),
				"Bonus Hp wasnt given for the cast of Heal");
		//test to see if the recipient needed spell missed out of range(took magic,no stat changed)		
		Server.getPlayers().get(1).addPositionX(-6);
		action= new SlotAction(Server.getPlayers().get(1),Server.getPlayers().get(2),'s',0,0,"Spearce",1,2);
		gameplay.executeSpell(action);
		//assert magic was taken for the spell
		assertEquals(new Warrior("Anon",-1).getMagic()+new LongSword().getMagic()+new Spearce().getMagic(),
				Server.getPlayers().get(1).getMagic(),
				"Magic wasnt taken for the cast of Spreace");
		//assert damage wasnt taken for the cast of spearce(out of range)
		assertEquals(new Dwarf("Anon",-1).getHp(),
				Server.getPlayers().get(2).getHp(),
				"Hp shouldnt be taken for the cast of Spreace (out of range)");
	}

	@Test
	void testExecuteMove() {
		setup = new Setup();
		setup.initialiseServerVariables();
		//test to see axis x and y changed appropriately
		SlotAction action= new SlotAction(Server.getPlayers().get(1),null,'m',2,-2,"",1,2);
		gameplay.executeMove(action);
		//assert the warrior moved +2 X axis
		assertEquals(15,
				Server.getPlayers().get(1).getPositionX(),
				"Player didnt move to the correct X");
		//assert the warrior moved -2 Y axis
		assertEquals(10,
				Server.getPlayers().get(1).getPositionY(),
				"Player didnt move to the correct Y");
	}

	@Test
	void testExecuteAttack() {
		setup = new Setup();
		setup.initialiseServerVariables();
		//test to see attack succeeded when in range
		SlotAction action= new SlotAction(Server.getPlayers().get(1),Server.getPlayers().get(0),'a',0,0,"",1,2);
		gameplay.executeAttack(action);
		assertEquals(new Mage("Anon",-1).getHp() - new Warrior("Anon",-1).getAttackDamage(),
				Server.getPlayers().get(0).getHp(),
				"Attack Damage wasnt dealt while in range");
		//test to see attack missed when out of range
		Server.getPlayers().get(2).addPositionX(3);
		action= new SlotAction(Server.getPlayers().get(1),Server.getPlayers().get(2),'a',0,0,"",1,2);
		gameplay.executeAttack(action);
		assertEquals(new Dwarf("Anon",-1).getHp(),
				Server.getPlayers().get(2).getHp(),
				"Attack Damage was dealt while out range");
	}
	
	@Test
	void testExecuteRest() {
	setup = new Setup();
	setup.initialiseServerVariables();
	//test to see player hp increased
	SlotAction action= new SlotAction(Server.getPlayers().get(1),null,'r',0,0,"",1,2);
	gameplay.executeRest(action);
	assertEquals(new Warrior("Anon",-1).getHp() + new Warrior("Anon",-1).getRest(),
			Server.getPlayers().get(1).getHp(),
			"Player didnt rest");
	}
	
	@Test
	void testExecuteCharge() {
		setup = new Setup();
		setup.initialiseServerVariables();
		//test to see player magic increased
		SlotAction action= new SlotAction(Server.getPlayers().get(1),null,'c',0,0,"",1,2);
		gameplay.executeCharge(action);
		assertEquals(new Warrior("Anon",-1).getMagic() + new Warrior("Anon",-1).getCharge(),
				Server.getPlayers().get(1).getMagic(),
				"Player didnt charge");
	}
	
	@Test
	void testExecuteCorrect() {
		//test to see if execution protocols are correct
	}

}
