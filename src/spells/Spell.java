package spells;

import console_version.Guides;
import console_version.SlotAction;
import network.Server;

public class Spell{
	private int Hp;
	private int Magic;
	private int AttackRange;
	private int AttackDamage;
	private int Movement;
	private int Rest;
	private int Charge;
	
	private int Duration;
	private int Range;
	private int Radius;
	private boolean isActive = false;
	private boolean isDisplayable = true;
	
	private double DamageVariation;
	
	private int recepient;
	private int caster;
	
	private String name;
	
	private boolean isRecepientNeeded = false;
	private boolean isCoordinateNeeded = false;
	private int castSlot;
	
	private int x;
	private int y;
	
	public Spell() {}

	public Spell(int hp, int magic, int attackRange, int attackDamage, int movement, int rest, int charge, int duration,
			int range, int radius, boolean isActive, boolean isDisplayable, double damageVariation, int recepient, int caster,
			String name, boolean needsRecepient,int x,int y) {
		Hp = hp;
		Magic = magic;
		AttackRange = attackRange;
		AttackDamage = attackDamage;
		Movement = movement;
		Rest = rest;
		Charge = charge;
		Duration = duration;
		Range = range;
		Radius = radius;
		this.isActive = isActive;
		this.isDisplayable = isDisplayable;
		DamageVariation = damageVariation;
		this.recepient = recepient;
		this.caster = caster;
		this.name = name;
		this.isRecepientNeeded = needsRecepient;
		this.x = x;
		this.y = y;
	}

	public int getCasterID() {
		return caster;
	}

	public void setCasterID(int caster) {
		this.caster = caster;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return Hp;
	}

	public void setHp(int hp) {
		Hp = hp;
	}

	public int getMagic() {
		return Magic;
	}

	public void setMagic(int magic) {
		Magic = magic;
	}

	public int getAttackRange() {
		return AttackRange;
	}

	public void setAttackRange(int attackRange) {
		AttackRange = attackRange;
	}

	public int getAttackDamage() {
		return AttackDamage;
	}

	public void setAttackDamage(int attackDamage) {
		AttackDamage = attackDamage;
	}

	public int getMovement() {
		return Movement;
	}

	public void setMovement(int movement) {
		Movement = movement;
	}

	public int getRest() {
		return Rest;
	}

	public void setRest(int rest) {
		Rest = rest;
	}

	public int getCharge() {
		return Charge;
	}

	public void setCharge(int charge) {
		Charge = charge;
	}

	public int getDuration() {
		return Duration;
	}

	public void setDuration(int duration) {
		Duration += duration;
	}
	
	public void setPureDuration(int duration) {
		Duration = duration;
	}

	public int getRange() {
		return Range;
	}

	public void setRange(int range) {
		Range = range;
	}

	public int getRadius() {
		return Radius;
	}

	public void setRadius(int radius) {
		Radius = radius;
	}

	public double getDamageVariation() {
		return DamageVariation;
	}

	public void setDamageVariation(double damageVariation) {
		DamageVariation = damageVariation;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDisplayable() {
		return isDisplayable;
	}


	public void setDisplayable(boolean isDisplayable) {
		this.isDisplayable = isDisplayable;
	}


	public int getRecepientID() {
		return recepient;
	}

	public void setRecepientID(int recepient) {
		this.recepient = recepient;
	}
	
	public void setInvolvedIDs(int casterID,int recepientID) {
		setCasterID(casterID);
		setRecepientID(recepientID);
	}
	
	public boolean isMagicEnough(int playerMagic) {
		//check if magic is enough
		return isActive() && playerMagic>=Math.abs(getMagic());
	
	}
	
	public boolean isHpEnough(int playerHp) {
		
		return  isActive() && playerHp>Math.abs(getHp());
	}
	
	public boolean isRecepientNeeded() {
		return isRecepientNeeded;
	}

	public void setRecepientNeeded(boolean needsRecepient) {
		this.isRecepientNeeded = needsRecepient;
	}
	
	public int getCastSlot() {
		return castSlot;
	}

	public void setCastSlot(int castSlot) {
		this.castSlot = castSlot;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isCoordinateNeeded() {
		return isCoordinateNeeded;
	}

	public void setCoordinateNeeded(boolean isCoordinateNeeded) {
		this.isCoordinateNeeded = isCoordinateNeeded;
	}

	public void broadcast(boolean isSuccess) {
		if(isSuccess) {
			Server.broadcast(new Guides().successfulSmarcText(new SlotAction(this)));
		}else {
			Server.broadcast(new Guides().failedSmarcText(new SlotAction(this)));
		}
	}
	
	public void cast(int casterID,int castSlot) {
		setCastSlot(castSlot);
	}
	public void cast(int casterID, int recepientID,int castSlot) {
		setCastSlot(castSlot);
	}
	public void cast(int casterID, int recepientID,boolean fromOtherSpell,int castSlot) {
		setCastSlot(castSlot);
	}
	
	public void cast(int casterID,int recepientID,int x,int y,int castSlot) {
		setCastSlot(castSlot);
	}
	public void run() {
	}


}
