package spells;

import network.Server;

public class Grind extends Spell{
	boolean hasRan = false;
	boolean isDeactivated = true;
	
	public Grind() {
		super(-25,0,0,-50,0,0,40,0,
				0,0,false,true,0.0,0,0,
				"Grind",false,0,0);	
	}
	
	public void cast(int casterID,int castSlot) {
		super.cast(casterID, castSlot);
				
		if(super.isActive()) {
			super.setActive(false);
			deactivate();
		}else {
			super.setInvolvedIDs(casterID, casterID);
			super.setActive(true);
			hasRan = false;
			run();
		}
		
	}
	
	public void run(){
		//check if magic is enough and spell is active
		if(super.isHpEnough(Server.getPlayers().get(getRecepientID()).getHp())) {
			isDeactivated = false;
			//reduce damage attack once
			if(!hasRan) {
				Server.getPlayers().get(super.getRecepientID()).setAttackDamage(super.getAttackDamage());
				Server.getPlayers().get(super.getRecepientID()).setCharge(super.getCharge());
			}
			//decrease caster hp every turn its active
			Server.getPlayers().get(super.getCasterID()).setHp(super.getHp());
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			//check if player has movement speed
			if(Server.getPlayers().get(super.getCasterID()).getMovement() != 0) {
				//add the movement to what we already have
				super.setMovement(super.getMovement()-
						Server.getPlayers().get(super.getCasterID()).getMovement());
				//set the movement to 0
				Server.getPlayers().get(super.getCasterID()).pureSetMovement(0);
			}
			
			//check if its ran at all this turn
			hasRan = true;
			broadcast(true);
		}else {
			broadcast(false);
			deactivate();
		}
	}
	
	public void deactivate() {
		if(!isDeactivated) {
		Server.getPlayers().get(super.getRecepientID()).setAttackDamage(-super.getAttackDamage());
		Server.getPlayers().get(super.getRecepientID()).setCharge(-super.getCharge());
		Server.getPlayers().get(super.getRecepientID()).setMovement(-super.getMovement());
		super.setMovement(0);
		hasRan = false;
		isDeactivated = true;
		}
	}




}
