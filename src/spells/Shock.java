package spells;

import characters.Player;
import network.Server;

public class Shock extends Spell{
	
	public Shock() {
		super(-75,-100,0,0,0,0,0,0,
				0,3,false,true,0.0,0,0,
				"Shock",false,0,0);	
	}
	
	public void cast(int casterID,int castSlot) {
		super.cast(casterID, castSlot);
		super.setInvolvedIDs(casterID, casterID);
		super.setActive(true);
		run();
	}
	
	public void run() {
		if(super.isMagicEnough(Server.getPlayers().get(getCasterID()).getMagic())) {
			//takes magic whether spell is in range or not
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			broadcast(true);
			int xStart = Server.getPlayers().get(super.getCasterID()).getPositionX() - super.getRadius();
			int yStart = Server.getPlayers().get(super.getCasterID()).getPositionY() - super.getRadius();
			int end = super.getRadius() * 2;
			
			for(int x=xStart;x<=xStart+end;x++) {
				for(int y=yStart;y<=yStart+end;y++) {
					for(Player player:Server.getPlayers()) {
						if(player.getId() != super.getCasterID() && 
								player.getPositionX() == x && 
								player.getPositionY() == y) {
							Server.getPlayers().get(player.getId()).setHp(super.getHp());
						}
					}
				}
			}
			
		}
		super.setActive(false);
	}
	
}
