package spells;

import console_version.Guides;
import network.Server;

public class Spearce extends Spell{
	Guides guide = new Guides();
	
	public Spearce() {
		super(-60,-60,0,0,0,0,0,0,
				6,0,false,true,0.0,0,0,
				"Spearce",true,0,0);	
	}
	
	public void cast(int casterID,int recepientID ,int castSlot) {
		super.cast(casterID, recepientID, castSlot);
		super.setInvolvedIDs(casterID, recepientID);
		super.setActive(true);
		run();
	}
	
	public void run() {
		if(super.isMagicEnough(Server.getPlayers().get(getCasterID()).getMagic())) {
			//takes magic whether spell is in range or not
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			if(super.getRange() >=
					Server.gameplay.offset_pos(Server.getPlayers().get(super.getCasterID()),
							Server.getPlayers().get(super.getRecepientID()))) {
				Server.getPlayers().get(super.getRecepientID()).setHp(super.getHp());
				broadcast(true);
			}else {
				broadcast(false);
			}
		}
		super.setActive(false);
	}
	
}
