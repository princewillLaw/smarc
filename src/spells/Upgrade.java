package spells;

import network.Server;

public class Upgrade extends Spell{
	boolean hasRan = false;
	boolean isDeactivated = true;
	
	public Upgrade() {
		super(0,-35,2,0,-1,5,0,0,
				0,0,false,true,-0.25,0,0,
				"Upgrade",true,0,0);	
	}
	
	public void cast(int casterID,int recepientID,int castSlot) {
		super.cast(casterID, recepientID, castSlot);
		
		if(super.isActive()) {
			super.setActive(false);
			deactivate();
		}else {
			super.setInvolvedIDs(casterID, recepientID);
			super.setActive(true);
			hasRan = false;
			run();
		}
		
	}
	
	public void run(){
		//check if magic is enough and spell is active
		if(super.isMagicEnough(Server.getPlayers().get(getRecepientID()).getMagic())) {
			isDeactivated = false;
			//reduce damage variation and movement only once
			if(!hasRan) {
				Server.getPlayers().get(super.getRecepientID()).setDamageVariation(super.getDamageVariation());
				Server.getPlayers().get(super.getRecepientID()).setAttackRange(super.getAttackRange());
				Server.getPlayers().get(super.getRecepientID()).setRest(super.getRest());
			}
			//decrease caster magic every turn its active
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			
			//check if its ran at all this turn
			hasRan = true;
			broadcast(true);
		}else {
			deactivate();
			broadcast(false);
		}
	}
	
	public void deactivate() {
		if(!isDeactivated) {
			Server.getPlayers().get(super.getRecepientID()).setDamageVariation(-super.getDamageVariation());
			Server.getPlayers().get(super.getRecepientID()).setAttackRange(-super.getAttackRange());
			Server.getPlayers().get(super.getRecepientID()).setRest(-super.getRest());
		hasRan = false;
		isDeactivated = true;
		}
	}


}
