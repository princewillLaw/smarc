package spells;

import network.Server;

public class Berserk extends Spell{
	boolean hasRan = false;
	boolean isDeactivated = true;

	public Berserk() {
		super(-20,0,0,40,3,0,0,0,
				0,0,false,true,0.5,0,0,
				"Berserk",false,0,0);
	}
	
	public void cast(int casterID,int castSlot) {
		super.cast(casterID, castSlot);
		if(super.isActive()) {
			super.setActive(false);
			deactivate();
		}else {
			super.setInvolvedIDs(casterID, casterID);
			super.setActive(true);
			hasRan = false;
			run();
		}	
	}
	
	public void run() {
		//check if magic is enough and spell is active
		if(super.isMagicEnough(Server.getPlayers().get(getRecepientID()).getMagic())
				&& super.isHpEnough(Server.getPlayers().get(getRecepientID()).getHp())) {
			isDeactivated = false;
			//increase attack damage and movement only once
			if(!hasRan) {
				Server.getPlayers().get(super.getRecepientID()).setAttackDamage(super.getAttackDamage());
				Server.getPlayers().get(super.getRecepientID()).setMovement(super.getMovement());
				Server.getPlayers().get(super.getCasterID()).setDamageVariation(super.getDamageVariation());
			}
			//decrease caster hp and magic every turn its active
			Server.getPlayers().get(super.getCasterID()).setHp(super.getHp());
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			//check if its ran at all this turn
			hasRan = true;
			broadcast(true);
		}else {
			deactivate();
			broadcast(false);
		}
		
	}
	
	public void deactivate() {
		if(!isDeactivated) {
			Server.getPlayers().get(super.getRecepientID()).setAttackDamage(-super.getAttackDamage());
			Server.getPlayers().get(super.getRecepientID()).setMovement(-super.getMovement());
			Server.getPlayers().get(super.getCasterID()).setDamageVariation(-super.getDamageVariation());
			hasRan = false;
			isDeactivated = true;
			}
	}
}
