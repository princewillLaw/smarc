package spells;

import characters.Player;
import network.Server;

public class Teleport extends Spell {
	
	//TELEPORTS A HERO 15 RANGE FROM THE MAGE, TO A LOCATION 15 RANGE FROM THE MAGE, 3 TURNS LATER.
	
	boolean hasRan = false;
	public Teleport() {
		super(0,-150,0,0,0,0,0,0,
				15,0,false,true,0.0,0,0,
				"Teleport",true,0,0);
		super.setCoordinateNeeded(true);
	}
	
	public void cast(int casterID,int recepientID,int x,int y,int castSlot) {
		if (!super.isActive()) {
			super.setDuration(2);
			super.setX(x);
			super.setY(y);
			super.cast(casterID, recepientID, castSlot);
			super.setInvolvedIDs(casterID, recepientID);
			super.setActive(true);
			run();
		}else {
			deactivate();
		}
	}
	
	public void run() {
		//check if it hasn't run the first time
		if(!hasRan) {
		//check if magic is enough
		if(super.isMagicEnough(Server.getPlayers().get(super.getCasterID()).getMagic())) { 
				//take the magic needed
				Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
				//check if recepient is in range of caster
				if(super.getRange() < Server.gameplay.offset_pos(Server.getPlayers().get(super.getCasterID()),
						Server.getPlayers().get(super.getRecepientID()))) {
					deactivate();
				}
				hasRan = true;
			}else {
				deactivate();
			}
		}
		//take a duration away
		super.setDuration(-1);
		//if duration finishes
		if(super.getDuration()<0) {
			//create a temp player to hold x and y teleport coordinates
			Player tLocale = new Player();
			tLocale.setPositionX(super.getX());
			tLocale.setPositionY(super.getY());
			//check if caster is not too far from coordinates
			if(super.getRange() >= Server.gameplay.offset_pos(Server.getPlayers().get(super.getCasterID()) ,tLocale)) {
				Server.getPlayers().get(super.getRecepientID()).setPositionX(super.getX());
				Server.getPlayers().get(super.getRecepientID()).setPositionY(super.getY());
				deactivate();
			}
		}
	}
	
	public void deactivate() {
		super.setActive(false);
		hasRan = false;
		super.setPureDuration(0);
		super.setX(0);
		super.setY(0);
	}

}
