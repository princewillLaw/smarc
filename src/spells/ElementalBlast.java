package spells;

import java.util.Random;
import network.Server;

public class ElementalBlast extends Spell{
	
	public ElementalBlast() {
		super(-50,-90,0,0,0,0,0,0,
				10,0,false,true,0.0,0,0,
				"Elemental Blast",true,0,0);	
	}
	
	public void cast(int casterID,int recepientID,int castSlot ) {
		super.cast(casterID, recepientID, castSlot);
		super.setInvolvedIDs(casterID, recepientID);
		super.setActive(true);
		run();
	}
	
	public void run() {
		if(super.isMagicEnough(Server.getPlayers().get(getCasterID()).getMagic())) {
			//takes magic whether spell is in range or not
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			if(super.getRange() >=
					Server.gameplay.offset_pos(Server.getPlayers().get(super.getCasterID()),
							Server.getPlayers().get(super.getRecepientID()))) {
				Server.getPlayers().get(super.getRecepientID()).setHp(super.getHp());
				randomizeElement();
				broadcast(true);
			}else {
				broadcast(false);
			}	
		}
		super.setActive(false);
	}
	
	public void randomizeElement() {
		int element = new Random().nextInt(4)+3;
		Server.getPlayers().get(super.getCasterID()).getSpells().get(element).cast(super.getCasterID(), super.getRecepientID(),true,super.getCastSlot());
		
	}

}
