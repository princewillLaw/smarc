package spells;

import network.Server;

public class WarCry extends Spell{
	boolean hasRan = false;
	boolean isDeactivated = true;
	
	public WarCry() {
		super(0,-35,0,20,2,0,0,0,
				0,0,false,true,0.0,0,0,
				"War Cry",false,0,0);	
	}
	
	public void cast(int casterID,int castSlot) {
		super.cast(casterID, castSlot);
		if(super.isActive()) {
			super.setActive(false);
			deactivate();
		}else {
			super.setInvolvedIDs(casterID, casterID);
			super.setActive(true);
			hasRan = false;
			run();
		}
		
	}
	
	public void run(){
		//check if magic is enough and spell is active
		if(super.isMagicEnough(Server.getPlayers().get(getRecepientID()).getMagic())) {
			isDeactivated = false;
			//reduce damage variation and movement only once
			if(!hasRan) {
				Server.getPlayers().get(super.getRecepientID()).setAttackDamage(super.getAttackDamage());
				Server.getPlayers().get(super.getRecepientID()).setMovement(super.getMovement());
			}
			//decrease caster magic every turn its active
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			
			//check if its ran at all this turn
			hasRan = true;
			broadcast(true);
		}else {
			deactivate();
			broadcast(false);
		}
	}
	
	public void deactivate() {
		if(!isDeactivated) {
		Server.getPlayers().get(super.getRecepientID()).setAttackDamage(-super.getAttackDamage());
		Server.getPlayers().get(super.getRecepientID()).setMovement(-super.getMovement());
		hasRan = false;
		isDeactivated = true;
		}
	}


}
