package spells;

import network.Server;

public class IceBlast extends Spell{
	boolean hasRan = false;
	public IceBlast() {
		super(0,0,0,0,-3,0,0,0,
				0,0,false,false,0.0,0,0,
				"Ice Blast",false,0,0);	
	}
	
	public void cast(int casterID,int recepientID,boolean fromElementalBlast,int castSlot) {
		super.cast(casterID, recepientID, fromElementalBlast, castSlot);
		if(fromElementalBlast) {
			super.setDuration(3);
			if(super.isActive()) {
				deactivate();
				cast(casterID,recepientID,fromElementalBlast,castSlot);
			}else {
				super.setInvolvedIDs(casterID, recepientID);
				super.setActive(true);
				hasRan = false;
			}
		}
	}
	
	public void run() {
		if(super.isActive() && super.getDuration() > 0 && !hasRan) {
			Server.getPlayers().get(super.getRecepientID()).setMovement(super.getMovement());
			hasRan = true;
			broadcast(true);
		}
		
		super.setDuration(-1);
		
		if(super.getDuration()<1) {
			deactivate();
		}
	}
	
	public void deactivate() {
		Server.getPlayers().get(super.getRecepientID()).setMovement(-super.getMovement());
		super.setActive(false);
	}

}
