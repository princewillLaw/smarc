package spells;

import network.Server;

public class EarthBlast extends Spell{

	public EarthBlast() {
		super(0,-15,0,0,0,0,0,0,
				0,0,false,false,0.0,0,0,
				"Earth Blast",false,0,0);		
	}
	
	public void cast(int casterID,int recepientID,boolean fromElementalBlast,int castSlot) {
		super.cast(casterID, recepientID, fromElementalBlast, castSlot);
		if(fromElementalBlast) {
			super.setDuration(3);
			super.setInvolvedIDs(casterID, recepientID);
			super.setActive(true);
		}
	}
	
	public void run() {
		if(super.isActive() && super.getDuration() > 0) {
			Server.getPlayers().get(super.getRecepientID()).setMagic(super.getMagic());
			broadcast(true);
		}
		
		super.setDuration(-1);
		
		if(super.getDuration()<1) {
			super.setActive(false);
		}
	}
}
