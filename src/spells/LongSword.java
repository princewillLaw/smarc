package spells;

import network.Server;

public class LongSword extends Spell{
	boolean hasRan = false;
	boolean isDeactivated = true;

	public LongSword() {
		super(0,-30,2,0,0,0,0,0,
			0,0,false,true,0.0,0,0,
			"Long Sword",false,0,0);
	}
	
	
	public void cast(int casterID,int castSlot) {
		super.cast(casterID, castSlot);
		if(super.isActive()) {
			super.setActive(false);
			deactivate();
		}else {
			super.setInvolvedIDs(casterID, casterID);
			super.setActive(true);
			hasRan = false;
			run();
		}	
	}
	
	public void run() {
		//check if magic is enough and spell is active
		if(super.isMagicEnough(Server.getPlayers().get(getRecepientID()).getMagic())) {
			isDeactivated = false;
			//increase attack range only once
			if(!hasRan) {
				Server.getPlayers().get(super.getRecepientID()).setAttackRange(super.getAttackRange());
			}
			//decrease caster magic every turn its active
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			broadcast(true);
			//check if its ran at all this turn
			hasRan = true;
		}else {
			deactivate();
			broadcast(false);
		}
	}
	
	public void deactivate() {
		if(!isDeactivated) {
			Server.getPlayers().get(super.getRecepientID()).setAttackRange(-super.getAttackRange());
		hasRan = false;
		isDeactivated = true;
		}
}

}
