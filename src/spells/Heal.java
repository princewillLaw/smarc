package spells;

import console_version.*;
import network.Server;

public class Heal extends Spell{
	Guides guide = new Guides();
	
	public Heal() {
		super(40,-75,0,0,0,0,0,0,
				10,0,false,true,0.0,0,0,
				"Heal",true,0,0);	

	}
	
	public void cast(int casterID,int recepientID,int castSlot ) {
		super.cast(casterID, recepientID, castSlot);
		super.setInvolvedIDs(casterID, recepientID);
		super.setActive(true);
		run();
	}
	
	public void run() {
		if(super.isMagicEnough(Server.getPlayers().get(getCasterID()).getMagic())) { 
			Server.getPlayers().get(super.getCasterID()).setMagic(super.getMagic());
			if( super.getRange() >= 
					Server.gameplay.offset_pos(Server.getPlayers().get(super.getCasterID()),
							Server.getPlayers().get(super.getRecepientID()))) {
			
			Server.getPlayers().get(super.getRecepientID()).setHp(super.getHp());
			
			broadcast(true);
		}else {
			broadcast(false);
		}
		super.setActive(false);
	}
		}

}
